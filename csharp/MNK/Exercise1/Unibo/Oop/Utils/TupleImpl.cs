﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly Object[] items;

        public TupleImpl(object[] args)
        {
            this.items = args;
        }

        public object this[int i] => items[i];

        public int Length => items.Length;

        public object[] ToArray()
        {
            Object[] arr = new Object[items.Length];
            Array.Copy(items, arr , items.Length);
            return arr;
        }

        public override bool Equals(Object obj)
        {
            if (obj != null)
            {
                if (obj is TupleImpl)
                {
                    return true;
                    //if (items.SequenceEqual((System.Collections.Generic.IEnumerable<Object>)obj))
                    //{
                    //    return true;
                    //}
                }
                    
            }
            return false;
        }

        public override int GetHashCode()
        {
            return items.Length * 231 % 500;
        }

        public String toString()
        {
            return "CIAO";
        }
    }
}
