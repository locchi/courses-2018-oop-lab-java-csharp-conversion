using System.Collections.Generic;
using System.Linq;
using Unibo.Oop.Mnk.Ui.Console.Control;
using Unibo.Oop.Mnk.Ui.Console.View;

namespace Unibo.Oop.Mnk.Ui.Console
{
    public static class Program
    {
        private static IList<int> ParseArgs(IList<string> argList)
        {
            return new[] {"-m", "-n", "-k"}
                .Select(arg => argList.IndexOf(arg))
                .Select(i =>
                {
                    int value;
                    if (i >= 0 && int.TryParse(argList[i + 1], out value))
                    {
                        return value;
                    }
                    else
                    {
                        return 3;
                    }
                    
                }).ToList();
        }

        public static void Main(params string[] args)
        {
            IList<int> parameters = ParseArgs(args.ToList());
            IMNKMatch match = MNKMatch.Of(parameters[0], parameters[1], parameters[2]);
            MNKConsoleView.Of(match);
            IMNKConsoleControl ctrl = MNKConsoleControl.Of(match);

            match.Reset();
            while (true) {
                ctrl.Input();
            }
        } 
        
    }
}